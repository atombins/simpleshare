GPP = gcc
SOURCES = $(wildcard src/*.c lib/*.c)
OBJ = $(patsubst %.c,obj/%.o,$(SOURCES))
OBJDIRS = $(dir $(OBJ))
CFLAGS = -O

.PHONY: clean build

$(shell mkdir -p $(OBJDIRS))

simpleshare: $(OBJ)
	$(GPP) $(CFLAGS) $^ -o $@

obj/%.o: %.c
	$(GPP) -c $(CFLAGS) -o $@ $^

clean:
	rm -rf obj
