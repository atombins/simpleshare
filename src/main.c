#include <stdio.h>

#include "../lib/tcpserver.h"
#include "../lib/tcpclient.h"

#include "dataCheck.h"

#define END_CONNECTION 0
#define MAINTAIN_CONNECTION 1

int read_to_buffer(FILE* file, char* buffer, unsigned buffer_size, uint64_t* data_size)
{
  *data_size = 0;
  while (fscanf(file, "%c", &buffer[*data_size]) == 1)
  {
    *data_size += 1;
    if (buffer_size == *data_size) return 0;
  }
  return 1;
}

int scan_to_buffer(char* buffer, unsigned buffer_size, uint64_t* data_size)
{
  *data_size = 0;
  while (scanf("%c", &buffer[*data_size]) == 1)
  {
    *data_size += 1;
    if (buffer_size == *data_size) return 0;
  }
  return 1;
}

void write_data(FILE* file, char* buffer, unsigned size)
{
  for (unsigned i = 0;i < size;i++)
  {
    fprintf(file, "%c", buffer[i]);
  }
}

void print_data(char* buffer, unsigned size)
{
  for (unsigned i = 0;i < size;i++)
  {
    printf("%c", buffer[i]);
  }
}

// delete from final relese
void DEBUG_SHOW_BUFFER(char* buffer, int size)
{
  for (unsigned i = 0;i < size;i++)
  {
    printf("%x ", buffer[i]);
  }
  printf("\n");
}

void uintToChars(char* ch, uint64_t num)
{
  char* tmp = (char*)&num;
  for (unsigned i = 0;i < 8;i++)
  {
    ch[i] = tmp[i];
  }
}

uint64_t charsToUint(char* ch)
{
  uint64_t ret;
  char* tmp = (char*)&ret;
  for (unsigned i = 0;i < 8;i++)
  {
    tmp[i] = ch[i];
  }
  return ret;
}

int call_download(Flags* flags)
{

  SOCK_TCP_Client* client = SOCK_TCP_Client_Start();
  if (client == NULL)
  {
    printf("Unable to create client.\n");
    return 1;
  }
  if (SOCK_TCP_Client_ConnectToServer(client, flags->ip_address, flags->port))
  {
    printf("Unable to connect the server.\n");
    SOCK_TCP_Client_Stop(client);
    return 1;
  }
  char* buffer = (char*)malloc(sizeof(char)*flags->buffer_size);
  FILE* file;
  if (flags->file != NULL) file = fopen(flags->file, "w");
  uint64_t data_size;
  char server_flag = MAINTAIN_CONNECTION;
  while (server_flag == MAINTAIN_CONNECTION)
  {
    SOCK_ReadData(client->sock, buffer, 9);
    server_flag = buffer[0];
    data_size = charsToUint(&buffer[1]);
    while (data_size > flags->buffer_size)
    {
      data_size -= flags->buffer_size;
      SOCK_ReadData(client->sock, buffer, flags->buffer_size);
      if (flags->file != NULL) write_data(file, buffer, flags->buffer_size);
      else print_data(buffer, flags->buffer_size);
    }
    SOCK_ReadData(client->sock, buffer, data_size);
    if (flags->file != NULL) write_data(file, buffer, data_size);
    else
    {
      print_data(buffer, data_size);
    }
  }
  if (flags->file != NULL) fclose(file);
  free(buffer);
  SOCK_TCP_Client_Stop(client);
  return 0;
}

int call_upload(Flags* flags)
{
  char err;
  char mask[9];
  uint64_t data_size = 0;
  SOCK_TCP_Server* server = SOCK_TCP_Server_Start(flags->port, 10, 1);
  if (server == NULL)
  {
    printf("Unable to start server. Try different port.\n");
  }
  char* buffer = (char*)malloc(sizeof(char)*flags->buffer_size);
  if (buffer == NULL)
  {
    SOCK_TCP_Server_Stop(server);
    printf("Unable to create buffer. Try smaller buffer size.\n");
    return 1;
  }
  FILE* file;
  if (flags->file != NULL) file = fopen(flags->file, "r");
  while (flags->upload_times > 0)
  {
    SOCK_TCP_Server_AcceptClient(server);
    if (flags->file != NULL) err = read_to_buffer(file, buffer, flags->buffer_size, &data_size);
    else err = scan_to_buffer(buffer, flags->buffer_size, &data_size);
    while (err == 0)
    {
      mask[0] = MAINTAIN_CONNECTION;
      uintToChars(&mask[1], data_size);
      SOCK_SendData(server->clients[0].sock, mask, sizeof(mask));
      SOCK_SendData(server->clients[0].sock, buffer, data_size);
      if (flags->file != NULL) err = read_to_buffer(file, buffer, flags->buffer_size, &data_size);
      else err = scan_to_buffer(buffer, flags->buffer_size, &data_size);
    }
    mask[0] = END_CONNECTION;
    uintToChars(&mask[1], data_size);
    SOCK_SendData(server->clients[0].sock, mask, sizeof(mask));
    SOCK_SendData(server->clients[0].sock, buffer, data_size);
    SOCK_TCP_Server_DisconnectClient(server, 0);
    if (flags->file != NULL) fseek(file, 0, SEEK_SET);
    flags->upload_times--;
  }
  if (flags->file != NULL) fclose(file);
  free(buffer);
  SOCK_TCP_Server_Stop(server);
  return 0;
}

int main(int argc, char *argv[])
{
  Flags flags;
  if (processFlags(&flags, argc, argv)) return 1;
  if (isNotOKflags(&flags)) return 1;
  if (flags.task == TASK_DOWNLOAD)
  {
    return call_download(&flags);
  }
  else if (flags.task == TASK_UPLOAD)
  {
    return call_upload(&flags);
  }
  return 1;
}
