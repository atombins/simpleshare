#include "flags.h"

void DEBUG_PrintFlag(Flags *flags)
{
  printf("\nFlags status:\n");
  if (flags->ip_address != NULL) printf("IP: %s\n", flags->ip_address);
  else printf("IP: (null)\n");
  printf("Port: %d\n", flags->port);
  printf("Task ID: %d\n", (int)flags->task);
  printf("Upload times remaining: %u\n", flags->upload_times);
  if (flags->file == NULL) printf("File: %s\n", flags->file);
  else printf("File: (null)\n");
  printf("Buffer size: %u\n", flags->buffer_size);
  printf("Force: %d\n", (int)flags->force);
}

const char* help_message =
"\n"
"This is small help for simpleshare program:\n"
"\n"
"parametres:\n"
"  -h --help    - Dispay this help\n"
"  -i -ip --ip  - Set connection target\n"
"  -p --port    - Set outgoing/ingoing port\n"
"                 <ip_address>:<port_number> is also valid as long as it is a valid way of\n"
"                 seting up ip address and port number\n"
"  -b --buffer  - Set size of buffer (default: 512 bytes)\n"
"  -f --file    - Set input/output file\n"
"  -t --task    - Set task to upload or download (default: download)\n"
"                 Usage:\n"
"                   -t <upload/download> or -t <1/0>\n"
"  -u --uptimes - Set how many time will be the file available\n"
"  --force      - Disable some safty checks\n";
void printHelp()
{
  printf("%s", help_message);
}

void setUpDefaultFlags(Flags *flags)
{
  flags->ip_address = NULL;
  flags->port = 0;
  flags->task = TASK_DOWNLOAD;
  flags->upload_times = 1;
  flags->file = NULL;
  flags->buffer_size = 512;
  flags->force = 0;
}

int processFlags(Flags *flags, int argc, char *argv[])
{
  setUpDefaultFlags(flags);
  int i, pos;
  for (i = 1;i < argc;i++)
  {
    if (argv[i][0] == '-')
    {
      // arg -h --help bring up help.
      if (strcmp("-h", argv[i]) == 0 || strcmp("--help", argv[i]) == 0)
      {
        printHelp();
        return 1; // do not process any other flags and end program.
      }
      // arg -p --port set specific Port
      else if (strcmp("-p", argv[i]) == 0 || strcmp("--port", argv[i]) == 0)
      {
        i++;
        if (isNumber(argv[i])) flags->port = atoi(argv[i]);
      }
      // arg -b --buffer set specific size of buffer
      else if (strcmp("-b", argv[i]) == 0 || strcmp("--buffer", argv[i]) == 0)
      {
        i++;
        if (isNumber(argv[i])) flags->buffer_size = atoi(argv[i]);
      }
      // arg -i --ip -ip set specific ip address
      else if (strcmp("-i", argv[i]) == 0 || strcmp("--ip", argv[i]) == 0 || strcmp("-ip", argv[i]) == 0)
      {
        i++;
        flags->ip_address = argv[i];
      }
      // -f --file set path to file
      else if (strcmp("-f", argv[i]) == 0 || strcmp("--file", argv[i]) == 0)
      {
        i++;
        flags->file = argv[i];
      }
      // -t --task set specific task
      else if (strcmp("-t", argv[i]) == 0 || strcmp("--task", argv[i]) == 0)
      {
        i++;
        if (strcmp("1", argv[i]) == 0 || strcmp("upload", argv[i]) == 0) flags->task = TASK_UPLOAD;
        else if (strcmp("0", argv[i]) == 0 || strcmp("download", argv[i]) == 0) flags->task = TASK_DOWNLOAD;
      }
      // -u --uptimes set how many times will be the file available
      else if (strcmp("-u", argv[i]) == 0 || strcmp("--uptimes", argv[i]) == 0)
      {
        i++;
        if (isNumber(argv[i])) flags->upload_times = atoi(argv[i]);
      }
      // --force disable some specific safty checks
      else if (strcmp("--force", argv[i]) == 0)
      {
        flags->force = 1;
      }
    }
    // for Franta user
    else
    {
      flags->ip_address = argv[i];
      for (pos = 0;argv[i][pos] != ':' && argv[i][pos] != '\0';pos++);
      if (argv[i][pos] == ':')
      {
        argv[i][pos] = '\0';
        if (isNumber(&argv[i][pos+1])) flags->port = atoi(&argv[i][pos+1]);
      }
    }
  }
  return 0;
}

int isNotOKflags(Flags *flags)
{
  int error_count = 0;
  if (flags->task == TASK_DOWNLOAD)
  {
    if (flags->port == 0)
    {
      error_count++;
      printf("Port number is not valid or unspecified!\n");
    }
    if (flags->ip_address == NULL)
    {
      error_count++;
      printf("IP address is not specified!\n");
    }
    else if (isNotIPv4(flags->ip_address))
    {
      error_count++;
      printf("%s is not valid IP address!\n", flags->ip_address);
    }
    if (flags->file != NULL)
    {
      FILE* file = fopen(flags->file, "w");
      if (file == NULL)
      {
        error_count++;
        printf("Unable to open %s!\n", flags->file);
      }
      else fclose(file);
    }
  }
  else if (flags->task == TASK_UPLOAD)
  {
    if (flags->port == 0)
    {
      error_count++;
      printf("Port number is not valid or unspecified!\n");
    }
    else if (flags->force == 0 && flags->port < 1024)
    {
      error_count++;
      printf("Specified port is lover than 1024!\n\nThat is dangerous these ports are usually default ports for other important programs.\n\nUse --force flag to igonre\nAlso do not forget to call this program as sudo for these ports.\n");
    }
    if (flags->upload_times == 0)
    {
      error_count++;
      printf("There have to be atleast 1 upload time.\n");
    }
    if (flags->file == NULL && flags->upload_times != 1)
    {
      error_count++;
      printf("You cannot send data streams repeatedly! :(\n");
    }
    else if (flags->file != NULL)
    {
      FILE* file = fopen(flags->file, "r");
      if (file == NULL)
      {
        error_count++;
        printf("Unable to open %s!\n", flags->file);
      }
      else fclose(file);
    }
  }
  if (flags->buffer_size < 16)
  {
    error_count++;
    printf("Buffer is too small!\n");
  }
  else if (flags->force == 0 && flags->buffer_size > 16384)
  {
    error_count++;
    printf("Buffer is dangerously big! You can disable this by --force flag\n");
  }
  return error_count;
}
