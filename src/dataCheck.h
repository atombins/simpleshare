#ifndef DATACHECK_H_INCLUDED
#define DATACHECK_H_INCLUDED

#include "flags.h"

/*
 * Checks if string is number
 *
 * Parametrs:
 *  char* number - potential number string
 *
 * Return:
 *  On number: 1
 *  If it is not number: 0
 */
char isNumber(char* number);

/*
 * Checks if string is IPv4 address
 *
 * Parametrs:
 *  char* ip - potential ip string
 *
 * Return:
 *  On valid ip address: 1
 *  If it is not ip address: 0
 */
char isNotIPv4(char* ip);

#endif // DATACHECK_H_INCLUDED
