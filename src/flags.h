#ifndef FLAGS_H_INCLUDED
#define FLAGS_H_INCLUDED

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "dataCheck.h"

#define TASK_DOWNLOAD 0
#define TASK_UPLOAD 1

typedef struct
{
  char* ip_address; // from where the file shoud be downloaded
  unsigned short port; // specifi port or random port
  char task; // upload file or download?
  unsigned int upload_times; // how meny times will be possible to download file
  char* file; // source file
  unsigned int buffer_size; // size of data per read/send in RAM
  char force; // do not preform some specific safety checks
} Flags;

// delete from final relese
void DEBUG_PrintFlag(Flags *flags);

/*
 * Show help.
 */
void printHelp();

/*
 * Set up default flags.
 *
 * Parametrs:
 *  Flags* flags - allocated flags
 */
void setUpDefaultFlags(Flags *flags);

/*
 * Load flags from *argv[]. And print errors.
 *
 * Parametrs:
 *  Flags *flags - allocated flags
 *  int argc - from main
 *  char *argv[] - from main
 *
 * Return:
 *  On succes: 0
 *  On fail: 1
 */
int processFlags(Flags *flags, int argc, char *argv[]);

/*
 * Check is there exist a way how to run the program with current flags setup. And print errors.
 *
 * Parametrs:
 *  Flags* flags - flags to check
 *
 * Return:
 *  On no error: 0
 *  On error: number of errors
 */
int isNotOKflags(Flags *flags);

#endif // FLAGS_H_INCLUDED
