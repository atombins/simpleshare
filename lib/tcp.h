#ifndef TCP_H
#define TCP_H

#include <stdlib.h>
#include <string.h>

#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <linux/sockios.h>
#include <arpa/inet.h>

#define EOK 0
#define ERROR 1

/*
 * Close all in and out going comunication on specific socket
 *
 * Parametrs:
 *  int sock - socket whitch will be closed
 */
 
void SOCK_CloseSocket(int sock);
/*
 * Check if there is any incomming data on socket
 *
 * Parametrs:
 *  int sock - on whitch socket
 *
 * Return:
 *  No incomming data: 0
 *  On incomming data: size of incoming data
 */
size_t SOCK_CheckSocketRead(int sock);

/*
 * // TO DEBUG: Check if working correctly.
 *
 * Check if there is any outgoing (not accepted on other side) data on socket
 *
 * Parametrs:
 *  int sock - on whitch socket
 *
 * Return:
 *  No outgoing data: 0
 *  On outgoing data: size of outgoing data
 */
size_t SOCK_CheckSocketSend(int sock);

/*
 * TO FIX: Sometimes sends data twice.
 *
 * Sends data tough specific socket with timeout.
 *
 * Parametrs:
 *  int sock - target socket
 *  void *data - pointer on any data type
 *  size_t size_of_data - size of data
 *  unsigned int timeout_count - how many tryes
 *  unsigned int timeout_delay - lenght of one delay in seconds
 *
 * Return:
 *  On succes: 0
 *  On fail: 1
 */
char SOCK_SendDataTimeout(int sock, void *data, size_t size_of_data, unsigned int timeout_count, unsigned int timeout_delay);

/*
 * Sends data tough specific socket.
 *
 * Parametrs:
 *  int sock - target socket
 *  void *data - pointer on any data type
 *  size_t size_of_data - size of data
 *
 * Return:
 *  On succes: 0
 *  On fail: 1
 */
char SOCK_SendData(int sock, void *data, size_t size_of_data);

/*
 * Wait for any data and read as much data as possible from specific socket.
 *
 * Parametrs:
 *  int sock - target socket
 *  void *data - pointer on buffer.
 *  size_t size_of_data - size of buffer (limit of possible dataload)
 *
 * Return:
 *  On succes: size of readed data
 *  On fail: -1
 */
int SOCK_ReadData(int sock, char* buffer, size_t buffer_size);

/*
 * Get ip address and port number of oponent on socket.
 *
 * Parametrs:
 *  char* ip - at lest 16 chars long pre allocated (including '\0' character), ip address will be returned here
 *  unsigned short *port - port number will be returned here
 *  int sock - on whitch socket
 *  struct sockaddr_in *address - client info
 *  int addrlen - size of cilent info
 */
int SOCK_GetOtherSideInfo(char* ip, unsigned short *port, int sock, struct sockaddr_in *address, int addrlen);

#endif // TCP_H
