#include "tcp.h"

void SOCK_CloseSocket(int sock)
{
    shutdown(sock, 2);
    close(sock);
}

size_t SOCK_CheckSocketRead(int sock)
{
  size_t data_size = 0;
  ioctl(sock, SIOCINQ, &data_size);
  return data_size;
}

size_t SOCK_CheckSocketSend(int sock)
{
  size_t data_size = 0;
  ioctl(sock, SIOCOUTQ, &data_size);
  return data_size;
}

char SOCK_SendDataTimeout(int sock, void *data, size_t size_of_data, unsigned int timeout_count, unsigned int timeout_delay)
{
  timeout_count--;
  for (size_t i = 0; i < timeout_count; i++) {
    if (send(sock , data , size_of_data, MSG_NOSIGNAL) == 1) return 0;
    sleep(timeout_delay);
  }
  if (send(sock, data, size_of_data, MSG_NOSIGNAL) == 0) return 0;
  return 1;
}

char SOCK_SendData(int sock, void *data, size_t size_of_data)
{
    if (send(sock, data, size_of_data, MSG_NOSIGNAL) == -1) return 1;
    return 0;
}

int SOCK_ReadData(int sock, char* buffer, size_t buffer_size)
{
    return read(sock, buffer, buffer_size);
}

int SOCK_GetOtherSideInfo(char* ip, unsigned short *port, int sock, struct sockaddr_in *address, int addrlen)
{
  if (getpeername(sock, (struct sockaddr *)address, &addrlen) < 0) return ERROR;
  strcpy(ip, inet_ntoa(address->sin_addr));
  *port = ntohs(address->sin_port);
  return EOK;
}
