#ifndef TCPSERVER_H
#define TCPSERVER_H

#include "tcp.h"

typedef struct
{
    struct sockaddr_in address; // client info
    int addrlen; // lenght of client info
    int sock; // client socket
    char is_up; // is it connected?
} SOCK_TCP_ClientInfo;

typedef struct
{
    int sock; // server accept socket
    struct sockaddr_in address; // definition of incoming address and protocols
    int addrlen; // lenght of address and protocols
    unsigned int max_connections; // max amount of connected clients
    SOCK_TCP_ClientInfo* clients; // field of connected clients
} SOCK_TCP_Server;

/*
 * Creates structure SOCK_TCP_Server and its substructures.
 *
 * Parametrs:
 *  unsigned shor port - in and out going port
 *  unsigned int request_count - size of waiting on accept queue
 *  unsigned int max_connections - max amount of connected clients
 *
 * Return:
 *  On success: address of created structure.
 *  On fail: NULL
 */
SOCK_TCP_Server* SOCK_TCP_Server_Start(unsigned short port, unsigned int request_count, unsigned int max_connections);

/*
 * Delete dynamicly allocated structure SOCK_TCP_Server and its substructures.
 *
 * Parametrs:
 *  SOCK_TCP_Server *the_server - allocated server structure
 */
void SOCK_TCP_Server_Stop(SOCK_TCP_Server *the_server);

/*
 * Safely remove client from clients field. Client have to get back in to accept queue for another comunication.
 *
 * Parametrs:
 *  SOCK_TCP_Server *the_server - allocated server structure
 *  unsigned int ID - Client index (starting from 0)
 */
void SOCK_TCP_Server_DisconnectClient(SOCK_TCP_Server *the_server, unsigned int ID);

/*
 * Server accept one incommig connection in accept queue and put in into clients field on first free index.
 *
 * Parametrs:
 *  SOCK_TCP_Server *the_server - allocated server structure
 *
 * Return:
 *  On success: 0
 *  On accept fail: 1
 *  On client count ower flow: 2
 */
int SOCK_TCP_Server_AcceptClient(SOCK_TCP_Server *the_server);

#endif // TCPSERVER_H
