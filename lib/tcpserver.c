#include "tcpserver.h"

SOCK_TCP_Server* SOCK_TCP_Server_Start(unsigned short port, unsigned int request_count, unsigned int max_connections)
{
    SOCK_TCP_Server* TheServer = (SOCK_TCP_Server*)malloc(sizeof(SOCK_TCP_Server));
    if (TheServer == NULL) return NULL;
    TheServer->max_connections = max_connections;
    TheServer->sock = 0;
    if ((TheServer->sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
        free(TheServer);
        return NULL;
    }
    int opt = 1;
    if (setsockopt(TheServer->sock, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt)))
    {
        shutdown(TheServer->sock, 2);
        free(TheServer);
        return NULL;
    }
    TheServer->addrlen = sizeof(TheServer->address);
    TheServer->address.sin_family = AF_INET;
    TheServer->address.sin_addr.s_addr = INADDR_ANY;
    TheServer->address.sin_port = htons(port);
    if (bind(TheServer->sock, (struct sockaddr *)&TheServer->address, sizeof(TheServer->address)) < 0)
    {
        shutdown(TheServer->sock, 2);
        free(TheServer);
        return NULL;
    }
    if (listen(TheServer->sock, request_count) < 0)
    {
        shutdown(TheServer->sock, 2);
        close(TheServer->sock);
        free(TheServer);
        return NULL;
    }
    TheServer->clients = (SOCK_TCP_ClientInfo*)malloc(sizeof(SOCK_TCP_ClientInfo)*max_connections);
    if (TheServer->clients == NULL)
    {
      shutdown(TheServer->sock, 2);
      close(TheServer->sock);
      free(TheServer);
      return NULL;
    }
    for (unsigned int i = 0;i < max_connections;i++)
    {
        TheServer->clients[i].is_up = 0;
        TheServer->clients[i].address.sin_family = AF_INET;
        TheServer->clients[i].address.sin_addr.s_addr = INADDR_ANY;
        TheServer->clients[i].address.sin_port = htons(port);
        TheServer->clients[i].addrlen = 0;
    }
    return TheServer;
}

void SOCK_TCP_Server_Stop(SOCK_TCP_Server *the_server)
{
    SOCK_CloseSocket(the_server->sock);
    for (unsigned int i = 0;i < the_server->max_connections;i++)
    {
        if (the_server->clients[i].is_up) SOCK_CloseSocket(the_server->clients[i].sock);
    }
    //free(the_server->clients);
    free(the_server);
}

void SOCK_TCP_Server_DisconnectClient(SOCK_TCP_Server *the_server, unsigned int ID)
{
    SOCK_CloseSocket(the_server->clients[ID].sock);
    the_server->clients[ID].is_up = 0;
}

int SOCK_TCP_Server_AcceptClient(SOCK_TCP_Server *the_server)
{
    unsigned int i = 0;
    while (i < the_server->max_connections)
    {
        if (the_server->clients[i].is_up==0) break;
        i++;
    }
    if (i == the_server->max_connections) return 2;
    if ((the_server->clients[i].sock = accept(the_server->sock, (struct sockaddr *)&the_server->clients[i].address, (socklen_t*)&the_server->clients[i].addrlen)) < 0) return 1;
    the_server->clients[i].is_up = 1;
    return EOK;
}
